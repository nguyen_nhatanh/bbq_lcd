#include "myFlash.h"

uint8_t section_active = 0;

int8_t SaveData(uint32_t pos, uint32_t data)
{
    uint16_t i = 0;
    uint32_t tmp_data = 0;
    uint32_t tmp_addr = 0;
    uint32_t actived = 0;
    uint32_t not_actived = 0;

    // check valid pos
    if (pos >255 || pos < 1)
    {
        return -1;
    }

    // check section is actived
    if (ReadFlash(HIGH_SECTION) == 1U)
    {
        actived = LOW_SECTION;
        not_actived = HIGH_SECTION;
    }
    else
    {
        actived = HIGH_SECTION;
        not_actived = LOW_SECTION;
    }
    // copy data
    for(i = 0; i < 256; i++)
    {
        if (i == 0)
        {
            tmp_data = 1; // set active this section
        }
        else if (i == pos)
        {
            tmp_data = data; // copy data
        }
        else
        {
            tmp_data = ReadFlash(actived + (i * 4));
        }
        tmp_addr = actived + (i *4);
        ProgramLongWord(tmp_addr, tmp_data);
    }
    // erase last section
    EraseSector(not_actived);
    return 0;
}

uint32_t LoadData(uint32_t pos)
{
    uint32_t actived = 0;

    if (pos < 1 || pos > 255)
    {
        return 0xffffffff;
    }

    if (ReadFlash(HIGH_SECTION) == 1U)
    {
        actived = LOW_SECTION;
    }
    else
    {
        actived = HIGH_SECTION;
    }

    return ReadFlash(actived + (pos * 4));
}

int8_t  ProgramLongWord(uint32_t Addr,uint32_t Data)
{
    uint32_t timeout = 0;
    // wait previous cmd complete
    while(CMD_STATUS == CMD_COMPLETE);

    // clear previous cmd error
    if(!((FTFA->FSTAT) == 0x80)) FTFA->FSTAT = 0x30;

    FTFA->FCCOB0 = CMD_PROGRAM_LONG_WORD ;

    // fill addr 24bit big endian
    FTFA->FCCOB1 = (uint8_t)((Addr >> 16) & 0xff) ;
    FTFA->FCCOB2 = (uint8_t)((Addr >> 8)  & 0xff) ;
    FTFA->FCCOB3 = (uint8_t)((Addr >> 0)  & 0xff) ;

    // fill data
    FTFA->FCCOB4 = (uint8_t)((Data >> 24)  & 0xff) ;
    FTFA->FCCOB5 = (uint8_t)((Data >> 16)  & 0xff) ;
    FTFA->FCCOB6 = (uint8_t)((Data >> 8 )  & 0xff) ;
    FTFA->FCCOB7 = (uint8_t)((Data >> 0 )  & 0xff) ;

    // execute cmd
    FTFA->FSTAT = 0x80;

    // wait cmd complete
    while(CMD_STATUS == CMD_COMPLETE)
    {
        __asm("nop");
        if (timeout++ > 1000)
        {
            return 0;
        }
    }
    return 1;
}

int8_t  EraseSector(uint32_t Addr)
{
    uint32_t timeout = 0;
    // wait previous cmd complete
    while(CMD_STATUS == CMD_COMPLETE);

    // clear previous cmd error
    if(!((FTFA->FSTAT) == 0x80)) FTFA->FSTAT = 0x30;

    FTFA->FCCOB0 = CMD_ERASE_SECTOR ;

    // fill addr 24bit big endian
    FTFA->FCCOB1 = (uint8_t)((Addr >> 16) & 0xff) ;
    FTFA->FCCOB2 = (uint8_t)((Addr >> 8)  & 0xff) ;
    FTFA->FCCOB3 = (uint8_t)((Addr >> 0)  & 0xff) ;

    // execute cmd
    FTFA->FSTAT = 0x80;

    // wait cmd complete
    while(CMD_STATUS == CMD_COMPLETE)
    {
        __asm("nop");
        if (timeout++ > 1000)
        {
            return 0;
        }
    }
    return 1;
}

int8_t  EraseMultiSector(uint32_t Addr,uint8_t nSize)
{
    uint8_t i = 0;
    uint8_t retVal = 1;

    for(i = 0; i < nSize;i++)
        retVal &= EraseSector(Addr + i*1024);

    return retVal;
}

uint32_t ReadFlash(uint32_t Addr)
{
    return *(__IO uint32_t*)Addr;
}