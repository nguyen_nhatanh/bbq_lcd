#ifndef __TIMER_H
#define __TIMER_H

#include "stdint.h"

#define MAX_TIMER_CREATE 16U

typedef void (*pfunction) (void*);

typedef enum _timer_status
{
    kCreateSucess = 0,
    kTimerFull,
    kAvaiableCallback
} timer_status_t;

typedef struct
{
    pfunction       CallbackFunc;
    uint32_t        TickCount;
    uint32_t        Interval;
    uint8_t         IsLoop;
    uint8_t         IsSuspend;
    uint8_t         Priority;
    void*           pData;
} CallbackFuncTypeDef;

typedef CallbackFuncTypeDef* timer_t;

timer_t CreateTimer(uint32_t interval, uint8_t isloop, pfunction _callback, void* _callback_data);
void CancelTimer(pfunction _callback);
void UpdateTimerTick(uint32_t _period);
uint32_t GetTick(void);
uint32_t GetTimerTick(void);
void TimerEventLoop(void);
void TimerIrqHandler(void);

uint8_t TimerIsRunning(timer_t timer_item);
void SuspendTimer(timer_t timer_item);
void ResumeTimer(timer_t timer_item);
void CancelTimerByItem(timer_t timer_item);
void ResetTimer(timer_t timer_item);

#endif
