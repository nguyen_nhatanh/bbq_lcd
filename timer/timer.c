#include "timer.h"
#include "stdlib.h"

volatile uint8_t TickChangeFlag = 0;
static CallbackFuncTypeDef ObjTimerArr[MAX_TIMER_CREATE] = {0};
static uint32_t tick_period = 5U; // default 10us
static volatile uint32_t g_tick_count = 0;
static uint32_t offset_tick = 1U;
//static uint8_t timer_error_flag = kCreateSucess;

timer_t CreateTimer(uint32_t interval, uint8_t isloop, pfunction _callback, void* _callback_data)
{
    uint8_t i = 0;

    for(i = 0; i < MAX_TIMER_CREATE; i++)
    {
        if(ObjTimerArr[i].CallbackFunc == NULL)
        {
            ObjTimerArr[i].CallbackFunc = _callback;
            if(interval < tick_period)
            {
                ObjTimerArr[i].TickCount = tick_period;;
            }
            else
            {
                ObjTimerArr[i].TickCount = interval;
            }
            ObjTimerArr[i].Interval = interval;
            ObjTimerArr[i].IsLoop = isloop;
            ObjTimerArr[i].IsSuspend = 0;
            ObjTimerArr[i].pData = _callback_data;
            return &ObjTimerArr[i];
        }
    }

    return NULL;
}

void ResetTimer(timer_t timer_item)
{
    timer_item->TickCount = timer_item->Interval;
}

uint32_t GetTick(void)
{
    return tick_period;
}

uint32_t GetTimerTick(void)
{
    return g_tick_count;
}

void CancelTimer(pfunction _callback)
{
    uint8_t i = 0;

    for(i = 0; i < MAX_TIMER_CREATE; i++)
    {
        if(ObjTimerArr[i].CallbackFunc == _callback)
        {
            ObjTimerArr[i].CallbackFunc = NULL;
            ObjTimerArr[i].TickCount = 0;
            ObjTimerArr[i].Interval = 0;
            ObjTimerArr[i].IsLoop = 0;
            ObjTimerArr[i].pData = NULL;
            break;
        }
    }
}

void CancelTimerByItem(timer_t timer_item)
{
    timer_item->CallbackFunc = NULL;
    timer_item->TickCount = 0;
    timer_item->Interval = 0;
    timer_item->IsLoop = 0;
    timer_item->pData = NULL;
}

void SuspendTimer(timer_t timer_item)
{
    timer_item->IsSuspend = 1;
}

uint8_t TimerIsRunning(timer_t timer_item)
{
    return !(timer_item->IsSuspend);
}

void ResumeTimer(timer_t timer_item)
{
    timer_item->IsSuspend = 0;
}

void UpdateTimerTick(uint32_t _period)
{
    if(_period > 10 && (_period % 10) == 0)
    {
        tick_period = _period;
        offset_tick = (uint32_t)((float)(tick_period) * 1.2f);
    }
}

__weak void StopTickCounter(void)
{

}

__weak void ResumeTickCounter(void)
{

}

void TimerIrqHandler(void)
{
    TickChangeFlag = 1;
    g_tick_count += (tick_period + offset_tick);
}

void TimerEventLoop(void)
{
    uint8_t i = 0;
    static pfunction callback_temp = NULL;
    static void* data_temp = NULL;

    if(TickChangeFlag)
    {
        StopTickCounter();

        TickChangeFlag = 0;

        for(i = 0; i < MAX_TIMER_CREATE; i++)
        {   if(ObjTimerArr[i].CallbackFunc == NULL) continue;
            if (!ObjTimerArr[i].IsSuspend)
            {
                if(ObjTimerArr[i].TickCount <= tick_period)
                {
                    ObjTimerArr[i].TickCount = 0;
                }
                else
                {
                    ObjTimerArr[i].TickCount -= (tick_period + offset_tick);
                }
            }
            if(ObjTimerArr[i].TickCount == 0 && callback_temp == NULL)
            {
                callback_temp = ObjTimerArr[i].CallbackFunc;

                data_temp = ObjTimerArr[i].pData;

                callback_temp(data_temp);

                if(!ObjTimerArr[i].IsLoop)
                {
                    ObjTimerArr[i].CallbackFunc = NULL;
                    ObjTimerArr[i].pData = NULL;
                    ObjTimerArr[i].TickCount = 0;
                    ObjTimerArr[i].Interval = 0;
                    ObjTimerArr[i].IsLoop = 0;
                }
                else
                {
                    ObjTimerArr[i].TickCount = ObjTimerArr[i].Interval;
                }

                callback_temp = NULL;
                data_temp = NULL;
            }
        }

        ResumeTickCounter();
    }
}
