#if !defined(__MYFLASH_H__)
#define __MYFLASH_H__

#include "fsl_common.h"

#define  CMD_COMPLETE                   (0x00)
#define  CMD_INPROGESS                  (0x01)

#define  CMD_PROGRAM_LONG_WORD          (0x06)
#define  CMD_ERASE_SECTOR               (0x09)
#define  CMD_READ_ONE                   (0x41)

#define  CMD_STATUS                     ((FTFA->FSTAT)&(1UL << 7))

#define  START_ADDR                     (0x00000000)
#define  END_ADDR                       (0x00010000)

#define  START_CONFIG_ADDR              (0x0000E000)
#define  HIGH_SECTION                   START_CONFIG_ADDR
#define  LOW_SECTION                    (START_CONFIG_ADDR + 1024)
#define  HIGH_SECTION_DATA              (HIGH_SECTION + 4)
#define  LOW_SECTION_DATA               (LOW_SECTION + 4)

#if defined(__cplusplus)
extern "C" {
#endif /* __cplusplus */

int8_t  ProgramLongWord(uint32_t Addr,uint32_t Data);
int8_t  EraseSector(uint32_t Addr);
int8_t  EraseMultiSector(uint32_t Addr,uint8_t nSize);
uint32_t ReadFlash(uint32_t Addr);

// save config api
int8_t SaveData(uint32_t pos, uint32_t data);
uint32_t LoadData(uint32_t pos);


#if defined(__cplusplus)
}
#endif /* __cplusplus */

#endif /* __MYFLASH_H__ */