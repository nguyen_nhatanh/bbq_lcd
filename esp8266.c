#include "esp8266.h"
#include "myADC.h"
#include "fsl_lpuart.h"
#include "fsl_port.h"
#include "fsl_rtc.h"
#include "board.h"
#include "timer.h"
#include "myUI.h"
#include "fsl_debug_console.h"

#define MAX_BUFFER_SIZE     256
#define MAX_CMD_PROCESS     16

static void ESP_Driven_Low(void* ptr);
void ESP_ResponseTimeOut(void* ptr);
void ESPSleepSend(void* ptr);
void ESPNormalSend(void* ptr);
void ESP_booting_wait(void* ptr);
void ESP_TimerTest(void* ptr);

static lpuart_config_t config;

static uint8_t g_ESPBuffer[MAX_BUFFER_SIZE] = {0};
static uint8_t g_buffer[64] = {0};
static lpuart_handle_t g_lpuartHandle;
static uint8_t esp8266_request = 0;
static uint8_t tx_transmit_finish = 1;
static uint8_t rx_recv_finish = 0;
static uint8_t last_cmd_id = CMD_NULL;
static uint8_t len_of_last_cmd = 0;
static size_t receivedBytes = 0;
static uint8_t wait_esp_response_finish = 0;

// var for send queue
#define MAX_QUEUE_SIZE      10
static queue_item_t sendQueue[MAX_QUEUE_SIZE] = {0};
static int8_t head_index = 0;
static int8_t tail_index = 0;

// var for transmit
question_t question = {
    .cmd_id = CMD_QUESTION,
    .question_data = 0x55
};

temp_profile_t temp_profile = {
    .cmd_id = CMD_TEMP_PROFILE,
    .temp_probe[0] = 75,
    .temp_probe[1] = 75,
    .temp_probe[2] = 75,
    .temp_probe[3] = 75
};

monitor_state_t monitor_state = {
    .cmd_id = CMD_MONITOR_START,
    .state = kStopMonitor
};

clock_t esp_datetime = {
    .cmd_id = CMD_GET_CLOCK,
    .year = 16,
    .month = 1,
    .day = 1,
    .hour = 1,
    .minute = 1,
    .second = 1,
};

interval_profile_t heart_beep_interval = {
    .cmd_id = CMD_GET_INTERVAL,
    .interval = 30
};

static command_t recv_cmd[MAX_CMD_PROCESS] = {0};

uint8_t esp_boot_done = 0;

extern alarm_done_t alarm_mess;
extern rtc_datetime_t datetime;

timer_t esp_check_response_timer = NULL;

/* LPUART user callback */
void LPUART_UserCallback(LPUART_Type *base, lpuart_handle_t *handle, status_t status, void *userData)
{
    if (kStatus_LPUART_TxIdle == status)
    {
        CreateTimer(50, 0 , ESP_Driven_Low, NULL); // finish transmit
    }

    if (kStatus_LPUART_RxIdle == status)
    {
        rx_recv_finish = 1;
    }
}

void ESP_Init(void)
{
    LPUART_GetDefaultConfig(&config);
    config.enableTx = true;
    config.enableRx = true;

    CLOCK_SetLpuart0Clock(1);

    LPUART_Init(LPUART0, &config, CLOCK_GetFreq(kCLOCK_McgIrc48MClk));
    LPUART_TransferCreateHandle(LPUART0, &g_lpuartHandle, LPUART_UserCallback, NULL);

    // init by cmd
    PushDataToQueue((void*)&monitor_state, sizeof(monitor_state_t), CMD_NULL);
    PushDataToQueue((void*)&temp_profile, sizeof(temp_profile_t), temp_profile.cmd_id);
    PushDataToQueue((void*)&heart_beep_interval, sizeof(interval_profile_t), heart_beep_interval.cmd_id);

    CreateTimer(2000, 0, ESP_booting_wait, NULL);
    //CreateTimer(1000, 1, ESP_TimerTest, NULL);
    esp_check_response_timer = CreateTimer(2000, 1, ESP_ResponseTimeOut, NULL);

    SuspendTimer(esp_check_response_timer);
}


void ESP_booting_wait(void* ptr)
{
    esp_boot_done = 1;
    LPUART_TransferStartRingBuffer(LPUART0, &g_lpuartHandle, g_ESPBuffer, MAX_BUFFER_SIZE);
}


void ESP_ReInit(void)
{
    //LPUART_GetDefaultConfig(&config);
    //config.enableTx = true;
    //config.enableRx = true;
    CLOCK_SetLpuart0Clock(1);
    LPUART_Init(LPUART0, &config, CLOCK_GetFreq(kCLOCK_McgIrc48MClk));
    LPUART_TransferCreateHandle(LPUART0, &g_lpuartHandle, LPUART_UserCallback, NULL);
    LPUART_TransferStartRingBuffer(LPUART0, &g_lpuartHandle, g_ESPBuffer, MAX_BUFFER_SIZE);
}

void ESP_Deinit(void)
{
    LPUART_TransferStopRingBuffer(LPUART0, &g_lpuartHandle);
    LPUART_Deinit(LPUART0);
}

void ESP_FinishTransmit(void* ptr)
{
    tx_transmit_finish = 1;
}

void ESP_Driven_Low(void* ptr)
{
    ESP_PIN_INDICATE(0);
    LPUART_FlushDataInRingBuffer(LPUART0, &g_lpuartHandle);
    CreateTimer(50, 0 , ESP_FinishTransmit, NULL); // finish transmit
}

uint8_t ESP_GetTransmitState(void)
{
    return (tx_transmit_finish && (!wait_esp_response_finish));  //
}

void ESP_RequestDataOnStart(void)
{

}

void ESP_SendData(uint8_t* buff, uint8_t len)
{

}

void ESP_RecvData(uint8_t* buff, uint8_t len)
{

}

uint8_t GetQueueSize(void)
{
    if (head_index < tail_index)
    {
        return (head_index + MAX_QUEUE_SIZE - tail_index);
    }
    else
    {
        return head_index - tail_index;
    }
}

void PushDataToQueue(void* ptr, uint16_t len, uint8_t cmd_id)
{
    if (GetQueueSize() == MAX_QUEUE_SIZE)
    {
        return;
    }

    sendQueue[head_index].len = len;
    sendQueue[head_index].ptrData = ptr;
    sendQueue[head_index].cmd_id = cmd_id;

    if (head_index == (MAX_QUEUE_SIZE - 1))
    {
        head_index = 0;
    }
    else
    {
        head_index++;
    }
}

queue_item_t* GetDataFromQueue(void)
{
    queue_item_t* item_tmp;

    if (GetQueueSize() > 0)
    {
        item_tmp = &sendQueue[tail_index];

        len_of_last_cmd = item_tmp->len;
        last_cmd_id = item_tmp->cmd_id;

        if (tail_index == (MAX_QUEUE_SIZE - 1))
        {
            tail_index = 0;
        }
        else
        {
            tail_index++;
        }
    }

    return item_tmp;
}

void ESPProcessSend(void *ptr) // Fix me
{
    if (ESP_SLEEP_STT()) // esp is sleeping
    {
        DBG("Sleed send %d", last_cmd_id);
        ESPSleepSend(ptr);

    }
    else
    {
        DBG("Normal send %d", last_cmd_id);
        ESPNormalSend(ptr);
    }
}

void ESPNormalSend(void* ptr)
{
    lpuart_transfer_t sendXfer;
    static uint8_t normal_state_send = 0;

    switch(normal_state_send)
    {
        case 0:
            tx_transmit_finish = 0;
            ESP_PIN_INDICATE(1);// driven high to start send data
            CreateTimer(10, 0, ESPNormalSend, ptr);
            normal_state_send++;
            break;
        case 1:
            sendXfer.data = (uint8_t*)(((queue_item_t*)ptr)->ptrData);
            sendXfer.dataSize = ((queue_item_t*)ptr)->len;
            LPUART_TransferSendNonBlocking(LPUART0, &g_lpuartHandle, &sendXfer);
            normal_state_send = 0;
            break;
    }
}

void ESPSleepSend(void* ptr)
{
    lpuart_transfer_t sendXfer;
    int i;
    static uint8_t sleep_state_send = 0;

    switch(sleep_state_send)
    {
        case 0:
            ESP_FORCE_WK(0); // start pulse reset esp to wake up
            for (i = 0; i < 3000; ++i) __asm("nop");
            ESP_FORCE_WK(1);
            CreateTimer(500, 0, ESPSleepSend, ptr);
            sleep_state_send++;
            break;
        case 1:
            tx_transmit_finish = 0; // set flag to indicate the transmition start

            // Check esp wakeup
            if (ESP_SLEEP_STT())
            {
                CreateTimer(100, 0, ESPSleepSend, ptr);
                return;
            }
            ESP_PIN_INDICATE(1);// driven high to start send data
            GPIOD->PSOR = 1 << 6;
            CreateTimer(10, 0, ESPSleepSend, ptr);
            sleep_state_send++;
            break;
        case 2:
            sendXfer.data = (uint8_t*)(((queue_item_t*)ptr)->ptrData);
            sendXfer.dataSize = ((queue_item_t*)ptr)->len;
            LPUART_TransferSendNonBlocking(LPUART0, &g_lpuartHandle, &sendXfer);
            sleep_state_send = 0;
            break;
    }
}

void ESP_TimerTest(void* ptr)
{
    DBG("Timer test command id %d", last_cmd_id);
}

void ESP_ResponseTimeOut(void* ptr)
{
    SuspendTimer(esp_check_response_timer);
    ResetTimer(esp_check_response_timer);
    LPUART_TransferAbortReceive(LPUART0, &g_lpuartHandle);
    DBG("Timeout reciver command id %d", last_cmd_id);
    wait_esp_response_finish = 0;
}

void ESP_CmdRegister(uint8_t id, pfunc handler)
{
    static uint8_t current_index = 0;
    if (current_index < MAX_CMD_PROCESS)
    {
        recv_cmd[current_index].cmd_id = id;
        recv_cmd[current_index].cmd_handler = handler;
        current_index++;
    }
}

uint8_t is_listen = 0;
uint8_t last_esp_state = 1;

void ESPProcess(void)
{
    static uint32_t time_wait_to_send = 0;
    uint8_t esp_state = 0;

    queue_item_t* tmp;
    lpuart_transfer_t receiveXfer;
    uint8_t i = 0;

    if(!esp_boot_done) return;

    esp_state = ESP_SLEEP_STT();

    if (!esp_state && esp_state ^ last_esp_state)
    {
        LPUART_FlushDataInRingBuffer(LPUART0, &g_lpuartHandle);
    }

    last_esp_state = esp_state;

    if(time_wait_to_send > 0) time_wait_to_send--;

    if ((time_wait_to_send == 0) && (GetQueueSize() > 0) && tx_transmit_finish && !is_listen) //
    {
        time_wait_to_send = 100;
        if (!wait_esp_response_finish)
        {
            tmp = GetDataFromQueue();
            DBG("Send last cmd_id %d, cmd_id %d, length %d", tmp->cmd_id, ((uint8_t*)(tmp->ptrData))[0], tmp->len);
            ESPProcessSend((void*)tmp);
            if (last_cmd_id != CMD_NULL)
            {
                DBG("Listen cmd_id %d", last_cmd_id);
                is_listen = 1;
            }
        }
    }

    if (tx_transmit_finish && is_listen)
    {
        is_listen = 0;
        DBG("Wait to reciver %d, length %d", last_cmd_id, len_of_last_cmd);
        last_cmd_id = CMD_NULL;
        if (last_cmd_id != NULL && len_of_last_cmd)
        {
            wait_esp_response_finish = 1;

            receiveXfer.data = g_buffer;
            receiveXfer.dataSize = len_of_last_cmd;
            len_of_last_cmd = 0;

            LPUART_TransferReceiveNonBlocking(LPUART0, &g_lpuartHandle, &receiveXfer, &receivedBytes);
            ResumeTimer(esp_check_response_timer);
        }
    }

    if (rx_recv_finish)
    {
        rx_recv_finish = 0;
        wait_esp_response_finish = 0;
        SuspendTimer(esp_check_response_timer);

        DBG("Reciver command id %d, length %d", g_buffer[0], receivedBytes);

        if (g_buffer[0] == CMD_ANSWER)
        {
            if (g_buffer[1] == CMD_TEMP_PROFILE)
            {
                PushDataToQueue((void*)&temp_profile, sizeof(temp_profile_t), temp_profile.cmd_id);
            }
            else if(g_buffer[1] == CMD_MONITOR_START)
            {
                monitor_state.state = kQuestionState;
                PushDataToQueue((void*)&monitor_state, sizeof(monitor_state_t), monitor_state.cmd_id);
            }
            else if (g_buffer[1] == CMD_GET_CLOCK)
            {
                PushDataToQueue((void*)&esp_datetime, sizeof(clock_t), esp_datetime.cmd_id);
            }
            else if(g_buffer[1] == CMD_ALARM_DONE)
            {
                ClearAlarm(NO_KEY_EVENT);
            }
        }
        else if (g_buffer[0] == CMD_TEMP_PROFILE)
        {
            temp_profile_t* tmp = (temp_profile_t*)(g_buffer);
            for (i = 0; i < 4; ++i)
            {
                temp_profile.temp_probe[i] = tmp->temp_probe[i];
                temp_profile.duration[i] = tmp->duration[i];
                DBG("Temprofile probe %d, temp %d, duration %d", i+ 1, temp_profile.temp_probe[i], temp_profile.duration[i]);
            }
        }
        else if (g_buffer[0] == CMD_WIFI_STATE)
        {

        }
        else if (g_buffer[0] == CMD_GET_CLOCK)
        {
            clock_t* tmp_clock = (clock_t*)(g_buffer);

            esp_datetime.year =     tmp_clock->year ;
            esp_datetime.month =    tmp_clock->month ;
            esp_datetime.day =      tmp_clock->day ;
            esp_datetime.hour =     tmp_clock->hour ;
            esp_datetime.minute =   tmp_clock->minute;
            esp_datetime.second =   tmp_clock->second;
            datetime.year   =  esp_datetime.year + 2000;
            datetime.month  =  esp_datetime.month;
            datetime.day    =  esp_datetime.day;
            datetime.hour   =  esp_datetime.hour;
            datetime.minute  = esp_datetime.minute;
            datetime.second  = esp_datetime.second;
            RTC_SetDatetime(RTC, &datetime);
            DBG("Year %d, month %d, day %d, hour %d, minute %d, second %d",datetime.year  ,datetime.month ,datetime.day   ,datetime.hour  ,datetime.minute,datetime.second);
        }
        else if (g_buffer[0] == CMD_MONITOR_START)
        {
            monitor_state_t* tmp_monitor = (monitor_state_t*)(g_buffer); //
            monitor_state.state = tmp_monitor->state;
        }
        else if (g_buffer[0] == CMD_GET_INTERVAL)
        {
            interval_profile_t* tmp_interval = (interval_profile_t*)(g_buffer); //

            heart_beep_interval.interval = tmp_interval->interval;
            DBG("Interval %d", heart_beep_interval.interval);
        }
        memset(g_buffer, 0 , 64);
        receivedBytes = 0;
    }
}

void ESPRequestInterrupt(void)
{
    if (ESP_REQUEST_STATUS())
    {
        ESP_REQUEST_STATUS_CLEAR();
        PushDataToQueue((void*)&question, 2, question.cmd_id);
    }
}
