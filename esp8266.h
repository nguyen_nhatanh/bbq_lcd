#ifndef _ESP8266_H_
#define _ESP8266_H_

#include "stdint.h"

#define DATA_SIZE 60

#define CMD_NULL                    255U
#define CMD_QUESTION                254U
#define CMD_ANSWER                  253U
#define CMD_TEMP                    1U
#define CMD_TEMP_PROFILE            2U
#define CMD_WIFI_CONTROL            3U
#define CMD_WIFI_STATE              4U
#define CMD_SELT_TEST               5U
#define CMD_MONITOR_START           6U
#define CMD_GET_CYCLE               7U
#define CMD_KEY_PRESS               8U
#define CMD_KEY_HOLD                9U
#define CMD_BATTERY                 10U
#define CMD_GET_CLOCK               11U
#define CMD_GET_SLEEP               12U
#define CMD_GET_INTERVAL            13U
#define CMD_BATTERY_LOW             14U
#define CMD_DEEP_SLEEP              15U // using the same structure with battery_low
#define CMD_LOW_TEMP                16U
#define CMD_TEMP_DONE               17U
#define CMD_TIME_DONE               18U
#define CMD_BURNNING                19U
#define CMD_ALARM_DONE              20U
#define CMD_GET_CALIB               21U

typedef enum {
    WIBBQ_WIFI_CLIENT = 0,
    WIBBQ_WIFI_SOFTAP,
    WIBBQ_WIFI_STANDALONE,
    WIBBQ_WIFI_MAX,
} wifi_mode_t;

typedef struct
{
    uint8_t cmd_id;
    uint8_t cmd_type;
}answer_t;

typedef struct
{
    uint8_t cmd_id;
    uint8_t question_data;
}question_t;

typedef struct
{
   uint8_t cmd_id;
   uint8_t reserve;
   uint16_t temp_probe[4];
   uint16_t duration[4];
} temp_profile_t;

typedef struct
{
    uint8_t cmd_id;
    uint8_t wifi_state;
}wifi_state_t;

typedef struct
{
    uint8_t cmd_id;
    uint8_t wifi_mode;
}wifi_control_t;

typedef struct
{
    uint8_t cmd_id;
    uint16_t len;
    void* ptrData;
} queue_item_t;

typedef struct
{
    uint8_t cmd_id;
    uint8_t power_status;
    uint8_t battery_percent;
    uint8_t reserve;
    uint32_t time_stamp;
    uint16_t probeVal[4];
} selftest_t;

typedef struct
{
    uint8_t  cmd_id;
    uint8_t  _resv;

    uint16_t bat_status;
    uint16_t bat_capacity;
} battery_t;

typedef struct
{
    uint8_t cmd_id;
    uint8_t state;
} monitor_state_t;

typedef struct
{
    uint8_t  cmd_id;
    uint8_t  _resv;

    uint8_t year; // Actual year - 2000
    uint8_t month;
    uint8_t day;
    uint8_t hour;
    uint8_t minute;
    uint8_t second;
} clock_t;

typedef struct
{
    uint8_t  cmd_id;
    uint8_t  _resv;
    uint16_t probe;

    uint32_t clock;

    uint32_t duration;

    uint32_t temp;
} burning_t;

typedef struct
{
    uint8_t  cmd_id;
    uint8_t  _resv;

    uint16_t cycle_time;
    uint16_t wait_time;
}cycle_t;

typedef struct
{
    uint8_t  cmd_id;
    uint8_t  _resv;

    uint16_t interval;
}interval_profile_t;

typedef struct
{
    uint8_t  cmd_id;
    uint8_t  _resv;
    uint16_t probe;

    uint16_t start_temp;
    uint16_t current_temp;

    uint32_t duration;

    uint32_t clock;
}low_temp_t;

typedef struct
{
    uint8_t  cmd_id;
    uint8_t  _resv;
    uint16_t probe;

    uint32_t clock;

    uint32_t duration;

    uint32_t temp;
} time_done_t;

typedef struct
{
    uint8_t  cmd_id;
    uint8_t  _resv;
    uint16_t probe;

    uint32_t clock;

    uint32_t duration;

    uint32_t temp;
}temp_done_t;

typedef struct
{
    uint8_t  cmd_id;
    uint8_t  alarm_id;
} alarm_done_t;

typedef struct
{
    uint8_t  cmd_id;
    uint8_t  _resv;
    uint16_t battery;
}battery_low_t;

typedef struct
{
    uint8_t cmd_id;
    uint8_t reserve;
    uint16_t reserve1;
    uint16_t calib_result[4];
} adc_calib_t;

enum e_monitor_statte
{
    kStopMonitor = 0,
    kStartMonitor = 1,
    kRestartMonitor = 2,
    kQuestionState = 255
};

typedef void (*pfunc) (void);

typedef struct
{
    uint8_t cmd_id;
    pfunc cmd_handler;
} command_t;

void ESP_Init(void);
void ESPProcess(void);
void ESPRequestInterrupt(void);
void SetDataToSend(void* ptr, uint8_t len);
void PushDataToQueue(void* ptr, uint16_t len, uint8_t cmd_id);
queue_item_t* GetDataFromQueue(void);
uint8_t GetQueueSize(void);
uint8_t ESP_GetTransmitState(void);
void ESP_Deinit(void);
void ESP_ReInit(void);
void ESP_CmdRegister(uint8_t id, pfunc handler);

#endif
